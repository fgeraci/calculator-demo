/**
* @author Fernando Geraci - Lockheed Martin example
* @version 1.0
*
* @section DESCRIPTION
*
* ExpressionEvaluator is the logic extendable engine of the calculator.
* It has been implemented with a Singleton desgin pattern to avoid waste since there could be only one instance
* at any given evaluating time on this simple implementation.
* The logic has been implemented as a grammar following simple rules to facilitate extendibility either in code
* or by subclassing ExpressionEvaluator - using ExpressionEvaluator::XYZ - 
*
*
*/


#define SUPPORTED_OPERATORS "+-*/;()|!%|"

class ExpressionEvaluator {

	public:
		double evaluate(string);
		static ExpressionEvaluator* getInstance();

	protected:
		// StringTokenizer *tokenizer;
		TokenBuffer *buffer;
		ExpressionEvaluator() {}; // regular constructor
		ExpressionEvaluator(const ExpressionEvaluator&) {}; // copy constructor
		ExpressionEvaluator& operator=(ExpressionEvaluator const&) {}; // override operator for avoiding copying
		~ExpressionEvaluator() {}; // destructor
		static ExpressionEvaluator* instance; // single static instance

		/* for grammar implementation */
		virtual double expression();
		virtual double term();
		virtual double primary();

};

/* SINGLETON INSTANTIATION HANDLING */

ExpressionEvaluator* ExpressionEvaluator::instance = 0;

ExpressionEvaluator* ExpressionEvaluator::getInstance() {
	// check for singleton instance reference
	if(!instance) {
		instance = new ExpressionEvaluator;
	}
	return instance;
}

/* FUNCTION IMPLEMENTATION */

double ExpressionEvaluator::evaluate(string expression) {
	
	string delimiters = SUPPORTED_OPERATORS;
	StringTokenizer tokenizer(expression,delimiters,true);
	this->buffer = new TokenBuffer(tokenizer);
	return this->expression();
}

/* GRAMMAR Concept

This grammar follows the following logic:

	Expresion:
		TERM
		TERM + TERM
		TERM - TERM
	Term:
		Primary
		Primary * TERM
		Primary / TERM
	Primary:
		Number
		'(' EXPRESSION ')'

It follows that for extendibility purposes, by respecting operators precedence, we can add functionality
by simply adding cases to this grammar implementation;
*/



double ExpressionEvaluator::primary() {
	Token token = buffer->getToken();
	double val = 0;
	if(token == "(") {
		val = expression();
		token = buffer->getToken();
		if(token == ")") return val;
		throw string("Malformed expression");
	} else if (token == "-") {
		token = buffer->getToken();
		val = atof(token.getVal().c_str());
		return (-1*val);
	} else {
		val = atof(token.getVal().c_str());
	}
}


double ExpressionEvaluator::term() {
	double left_val = primary();
	Token t = buffer->getToken();
	while(true) {
		if(t == "*") {
			left_val *= primary();
			t = buffer->getToken();
		} else if (t == "/") {
			double right_val = primary();
			if(right_val != 0) {
				left_val /= right_val;
				t = buffer->getToken();
			} else {
				throw string("Cannot divide by zero");
			}
		} else {
			buffer->setToken(t);
			return left_val;		
		}
	}
}

double ExpressionEvaluator::expression() {
	Token token = buffer->getToken();
	if(token.getVal() == ";") return 0;
	buffer->setToken(token);
	double left_val = term();
	token = buffer->getToken();
	while(true) {
		if(token == "+") {
			left_val += expression();
			token = buffer->getToken();
		} else if (token == "-") {
			left_val -= expression();
			token = buffer->getToken();		
		} else {
			if(token == ")") throw string("malformed error");
			buffer->setToken(token);
			return left_val;		
		}
	}
}


/**
	As example, ExpressionEvaluatorExtended will compute absolute values | |.

*/

class ExpressionEvaluatorExtended : public ExpressionEvaluator {
	public:
		static ExpressionEvaluatorExtended* getInstance(); 
	protected:
		using ExpressionEvaluator::expression;
		using ExpressionEvaluator::term;
		static ExpressionEvaluatorExtended* instance;
		double primary();
};


/* SINGLETON INSTANTIATION HANDLING */

ExpressionEvaluatorExtended* ExpressionEvaluatorExtended::instance = 0;

ExpressionEvaluatorExtended* ExpressionEvaluatorExtended::getInstance() {
	// check for singleton instance reference
	if(!ExpressionEvaluatorExtended::instance) {
		ExpressionEvaluatorExtended::instance = new ExpressionEvaluatorExtended;
	}
	return ExpressionEvaluatorExtended::instance;
}

/* FUNCTION IMPLEMENTATION */


double ExpressionEvaluatorExtended::primary() {
	Token token = buffer->getToken();
	double val = 0;
	if(token == "(") {
		val = expression();
		token = buffer->getToken();
		if(token == ")") return val;
		throw string("Malformed expression");
	} else if (token == "-") {
		token = buffer->getToken();
		val = atof(token.getVal().c_str());
		return (-1*val);
	} else if (token == "|") { 
		val = expression();
		token = buffer->getToken();
		if(token == "|") {
			if(val < 0) val *= -1;
			return val;		
		} 
		throw string("Malformed expression");
	} else {
		val = atof(token.getVal().c_str());
	}
}
