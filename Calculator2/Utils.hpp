
/**
*
* @file Utils - Lockheed Martin example
* @author Fernando Geraci
*
* @section DESCRIPTION
* Utils will provide support to logic engine and misc needs such as input parsing and buffering
* and other misc. handling.
*/

#define DEFAULT_DELIMITER " "


class StringTokenizer;
class Token;
class TokenBuffer;


class StringTokenizer {
	private:
		vector<Token> tokens;
		int currentIndex;
		bool includeDelimiters;
		string expression;
		string delimiters;
		void tokenize();
		bool isDelimiter(char);
	public:
		/* OVERLOADED CONSTRUCTORS */
		StringTokenizer(string expression, string delimiters, bool includeDelimiters):
		expression(expression),delimiters(delimiters),includeDelimiters(includeDelimiters), currentIndex(0) { tokenize(); }
		StringTokenizer(string expression, string delimiters) { StringTokenizer(expression,delimiters,false); }
		StringTokenizer(string expression) { StringTokenizer(expression, DEFAULT_DELIMITER, false); }
		StringTokenizer(string expression, bool includeDelimiter) { StringTokenizer(expression,DEFAULT_DELIMITER,true); }
		/*                         */
		Token next();
		bool hasNext();
};


class Token {
	private:
		string val;
	public:
		string getVal();
		Token() {};
		Token(string val):val(val) { }
	bool operator==(const string& op) {
		return this->val == op;
	}
};

string Token::getVal() {
	return val;
}

class TokenBuffer {
	public:
		TokenBuffer(StringTokenizer& tokenizer) {
			this->tokenizer = &tokenizer;
			full = false;
		}
		TokenBuffer() {
			full = false;
		}

		void setToken(Token);
		Token getToken();

	private:
		StringTokenizer* tokenizer;
		Token token;
		bool full;
};

void TokenBuffer::setToken(Token t) {
	if(!full) {	
		token = t;
		full = true;
		return;
	} throw string("Illegal buffer handling");
}

Token TokenBuffer::getToken() {
	Token t;
	if(full) {
		full = false;
		t = token;
	} else {
		t = tokenizer->next();
	}
	return t;
}




void StringTokenizer::tokenize() {
	int length = expression.length();
	string curr = "";
	for(int i = 0; i < length; ++i) {
		curr += expression[i];
		if(isDelimiter(curr[0])) {
			if(includeDelimiters) {
				tokens.push_back(Token(curr));
			}
		} else {
			string currentToken = "";
			while(true) {
				currentToken += curr;
				++i;
				if (i < length) {
					if(isDelimiter(expression[i])) {
						--i;
						tokens.push_back(Token(currentToken));
						break;
					} else {
						curr = expression[i];
					}
				} else {
					double val = atof(currentToken.c_str());
					tokens.push_back(Token(currentToken));
					return;
				}
			}
		}
		curr = "";
	}
}

bool StringTokenizer::isDelimiter(char ch) {
	int size = delimiters.length();
	for(int i = 0; i < size; ++i) {
		if (ch == delimiters[i]) return true;
	}
	return false;
}

Token StringTokenizer::next() {
	if(hasNext()) {
		Token tok = tokens.at(currentIndex);
		currentIndex++;
		return tok;
	}
	throw string("Illegal tokenizer access");
}

bool StringTokenizer::hasNext() {
	if(currentIndex < expression.length()) return true;
	return false;
}
