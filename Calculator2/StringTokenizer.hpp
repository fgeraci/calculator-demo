
#define DEFAULT_DELIMITER " "

class StringTokenizer {
	private:
		vector<Token> tokens;
		int currentIndex;
		bool includeDelimiters;
		string expression;
		string delimiters;
		void tokenize();
		bool isDelimiter(char);
	public:
		/* OVERLOADED CONSTRUCTORS */
		StringTokenizer(string expression, string delimiters, bool includeDelimiters):
		expression(expression),delimiters(delimiters),includeDelimiters(includeDelimiters), currentIndex(0) { tokenize(); }
		StringTokenizer(string expression, string delimiters) { StringTokenizer(expression,delimiters,false); }
		StringTokenizer(string expression) { StringTokenizer(expression, DEFAULT_DELIMITER, false); }
		StringTokenizer(string expression, bool includeDelimiter) { StringTokenizer(expression,DEFAULT_DELIMITER,true); }
		/*                         */
		Token next();
		bool hasNext();
};

void StringTokenizer::tokenize() {
	int length = expression.length();
	for(int i = 0; i < length; ++i) {
		string curr = "" + expression[i];
		if(isDelimiter(curr.at(0))) {
			if(includeDelimiters) {
				tokens.push_back(Token(curr));
			}
		} else {
			string currentToken = "";
			while(true) {
				currentToken += curr;
				++i;
				if (i < length) {
					if(isDelimiter(expression[i])) {
						--i;
						string c = "" + currentToken[0];
						tokens.push_back(Token(c));
						break;
					} else {
						curr = expression[i];
					}
				} else {
					double val = atof(currentToken.c_str());
					tokens.push_back(Token(currentToken));
					return;
				}
			}
		}
	}
}

bool StringTokenizer::isDelimiter(char ch) {
	int size = delimiters.length();
	for(int i = 0; i < size; ++i) {
		if (ch == delimiters[i]) return true;
	}
	return false;
}

Token StringTokenizer::next() {
	if(hasNext()) {
		Token tok = tokens.at(currentIndex);
		currentIndex++;
		return tok;
	}
	throw string("Illegal tokenizer access");
}

bool StringTokenizer::hasNext() {
	return 0;
}
