
/**
* @file CALCULATOR - Lockheed Martin example
* @author Fernando Geraci
* @version 1.0
*
* @section DESCRIPTION
*
* The following is a complete implementation of a fully extendable calculator.
* Although only a few operators where implemented, understanding the grammar behind
* the expression parsing enables the programmer to extend its functionality to other
* operators and operands.
*
* @section DESIGN
*
* The main idea behind the implementation is a grammar of the form:
* 	EXPRESSION= term || expression + expression || expression - expression
*	TERM= primary || term * primary || term / primary
*	PRIMARY= number || '(' <expression> ')' || uniary modifiers such as '-'
*
*
* Logic occurs in the singleton patter designed ExpressionEvaluator which utilizes
* Utils for misc functions.
*
* A StringTokenizer was implemented for managing buffering and Tokens retrieval.
*
* Two auxiliary classes were also implemented: TokenBuffer and Token. 
*/

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

/* headers */
#include "Utils.hpp"
#include "ExpressionEvaluator.hpp"

/* IMPLEMENTATION */

void printHelp() {
	cout << "\nEVALUATION FORMAT: <expression>;\n"
		<< "SUPPORTED OPERATORS: + - * / ( <expression> )\n\n";
}

void runner() {

	ExpressionEvaluator *evaluator = ExpressionEvaluator::getInstance();
	bool run = 1;
	string expression = "0";

	while(run) {
		cout << "> ";
		try {
			cin >> expression;
			if(expression == "q;") {
				run = false;
			} else if (expression == "?;") {
			 	printHelp();
			} else {
				cout << "= " << evaluator->evaluate(expression) << endl;
			}
		/* ALL ERRORS WILL BE CENTRALIZED INTO THIS CATCH BLOCK */
		} catch (...) { cout << "ERROR: malformed expression" << endl; }
	}
}

/* MAIN */

int main(int argc, char** argv) {

	cout 	<< "\nScientific calculator - Lockheed Martin Demo\n\n"
		<< "EVALUATION FORMAT: <expression>;\n"
		<< "SUPPORTED OPERATORS: + - * /\n\n"
		<< "run calculator -h or type ?; at runmode\n";
	// main program loop
	runner();
	cout << endl;
	return 0;

}



