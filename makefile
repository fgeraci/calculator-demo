calculator: Calculator.o
	g++ Calculator.o -o calculator && rm -rf *o

calculator.o: Calculator.cpp
	g++ -c Calculator.cpp

ExpressionEvaluator.o: ExpressionEvaluator.hpp

clean:
	rm -rf *o calculator
